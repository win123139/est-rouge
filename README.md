

# ToDoList

## Features

- Functions to Add, Edit, Delete Work. A work includes information of “Work Name”, “Starting Date”, “Ending Date” and “Status” (Planning, Doing, Complete)

- Function to show the works on a calendar view with: Date, Week, Month (can use third-party library)

## Requirements

- PHP 5.6 or PHP 7.0
- MySQL
- mod_rewrite activated 
- Composer


## Manual Installation

1. Edit the database credentials in `application/config/config.php`
2. Execute the est_calendar.sql statements in the `_install/`-folder (with PHPMyAdmin for example).
3. Make sure you have mod_rewrite activated on your server / in your environment.
4. Config your server apache, nginx with root folder is /public of project . ( see example config in Server configs below)
5. Install composer and run `composer install` in the project's folder to create the PSR-4 autoloading and codeception for unit test

## Manual Installation unit test

1. Edit the database credentials in `tests/unit.suite.yml`  with your database 
2. Execute the  unit test by run  `vendor/bin/codecept run unit` in root folder of project

if have problem please delete vendor folder ,composer.lock and run `composer install` againt
## Server configs for

### nginx

```nginx
server {
    server_name default_server _;   # Listen to any servername
    listen      [::]:80;
    listen      80;

    root /var/www/html/est-rouge/public;

    location / {
        index index.php;
        try_files /$uri /$uri/ /index.php?url=$uri&$args;
    }

    location ~ \.(php)$ {
        fastcgi_pass   unix:/var/run/php5-fpm.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```
### apache
```apache

<VirtualHost *:8001>

        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/est-rouge/public

        <Directory /var/www/html/est-rouge/public>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```
