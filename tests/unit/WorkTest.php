<?php

// set a constant that holds the project's folder path, like "/var/www/".
// DIRECTORY_SEPARATOR adds a slash to the end of the path
define('ROOT', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR);
// set a constant that holds the project's "application" folder, like "/var/www/application".
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);

// This is the auto-loader for Composer-dependencies (to load tools into your project).
require ROOT . 'vendor/autoload.php';

// load application config (error reporting etc.)
require APP . 'config/config.php';

use Mini\Model\Work;

class WorkTest extends \Codeception\Test\Unit {

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        
    }

    protected function _after() {
        
    }

    //  normal
    public function testValidation01() {
        $work = new Work();
        $result = $work->validate('Meeting', '2018-08-08 07:20:11', '2018-08-08 08:20:11', 1);

        $this->assertTrue($result);
    }

    // work_name require
    public function testValidation02() {
        $work = new Work();
        $result = $work->validate('', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);
        $this->assertFalse($result);
    }

    // work_name max 255 character
    public function testValidation03() {
        $work = new Work();
        $result = $work->validate('gLdc8TrNuYwPmDl2dRXlCoTDMaEuEzbqWFIOyD8dRIy9Vn5PUdG'
                . 'OlrFdUARRr8zdSBB1BAhrevIJU01q7cCf0bxMGLQtq8n37lSn4TuMLNt55KRwkOKaZToRJEApYejrHdJPqVlyzwzjsiO60U09rvT'
                . 'TLaTb8GW0gAjbNblQcfuKDGZYP1Uph0iDav8b6Kn3F9v6Yr4aj2yv9xsO96v5R0WDbD4'
                . 'm76R2cxBrre9rRm2oT083WiMGduvPNt35c5t', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);

        $this->assertTrue($result);
    }

    // work_name max 256 character
    public function testValidation04() {
        $work = new Work();
        $result = $work->validate('gLdc8TrNuYwPmDl2dRXlCoTDMaEuEzbqWFIOyD8dRIy9Vn5PUdG'
                . 'OlrFdUARRr8zdSBB1BAhrevIJU01q7cCf0bxMGLQtq8n37lSn4TuMLNt55KRwkOKaZToRJEApYejrHdJPqVlyzwzjsiO60U09rvT'
                . 'TLaTb8GW0gAjbNblQcfuKDGZYP1Uph0iDav8b6Kn3F9v6Yr4aj2yv9xsO96v5R0WDbD4'
                . 'm76R2cxBrre9rRm2oT083WiMGduvPNt35c5tM', '2018-08-08 07:20:11', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    // start_date require
    public function testValidation05() {
        $work = new Work();
        $result = $work->validate('Metting', '', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    // start_date not in format
    public function testValidation06() {
        $work = new Work();
        $result = $work->validate('Metting', '1234 a', '2018-08-08 07:20:11', 1);

        $this->assertFalse($result);
    }

    // end_date require
    public function testValidation07() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '', 1);

        $this->assertFalse($result);
    }

    // end_date not in format
    public function testValidation08() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '123 07:20:11', 1);

        $this->assertFalse($result);
    }

    // end_date before start_date
    public function testValidation09() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '2018-08-08 01:20:11', 1);

        $this->assertFalse($result);
    }

    // status not in 1,2,3
    public function testValidation10() {
        $work = new Work();
        $result = $work->validate('Metting', '2018-08-08 05:20:11', '2018-08-08 07:20:11', 4);

        $this->assertFalse($result);
    }

    // get all row in table
    public function testGetAllWork01() {
        $work = new Work();
        $result = count($work->getAllWorks());
        $number_row = $this->tester->grabNumRecords('tbl_work');
        $this->assertEquals($result, $number_row);
    }

    // add work to table
    public function testAddWork01() {
        $work = new Work();
        $work->addWork('Meeting', '2018-08-08 07:20:11', '2018-08-08 08:20:11', 1);
        $this->tester->seeInDatabase('tbl_work', ['work_name' => 'Meeting', 'start_date' => '2018-08-08 07:20:11', 'end_date' => '2018-08-08 08:20:11', 'status' => 1]);
    }

    // datele work in table
    public function testDeleteWork01() {
        $this->tester->haveInDatabase('tbl_work', ['id' => 10, 'work_name' => 'Meeting', 'start_date' => '2018-08-08 07:20:11', 'end_date' => '2018-08-08 08:20:11', 'status' => 1]);
        $work = new Work();
        $work->deleteWork(10);
        $this->tester->dontSeeInDatabase('tbl_work', ['id' => 10]);
    }

    // get work in table
    public function testGetWork01() {
        $this->tester->haveInDatabase('tbl_work', ['id' => 10, 'work_name' => 'Meeting', 'start_date' => '2018-08-08 07:20:11', 'end_date' => '2018-08-08 08:20:11', 'status' => 1]);
        $work = new Work();
        $result = $work->getWork(10);
        $this->assertEquals($result->id, 10);
        $this->assertEquals($result->work_name, 'Meeting');
        $this->assertEquals($result->start_date, '2018-08-08 07:20:11');
        $this->assertEquals($result->end_date, '2018-08-08 08:20:11');
        $this->assertEquals($result->status, 1);
    }

    // update work to table
    public function testUpdateWork01() {
        $this->tester->haveInDatabase('tbl_work', ['id' => 10, 'work_name' => 'Meeting', 'start_date' => '2018-08-08 07:20:11', 'end_date' => '2018-08-08 08:20:11', 'status' => 1]);

        $work = new Work();
        $work->updateWork(10, 'Meeting 2', '2018-08-08 08:20:11', '2018-08-08 09:20:11', 2);
        $this->tester->seeInDatabase('tbl_work', ['work_name' => 'Meeting 2', 'start_date' => '2018-08-08 08:20:11', 'end_date' => '2018-08-08 09:20:11', 'status' => 2]);
    }

    // get number of work not complete 
    public function testAmountOfWorksNotComplete01() {

        $work = new Work();
        $result = $work->getAmountOfWorksNotComplete();
        $number_work = $this->tester->grabNumRecords('tbl_work', ['status <' => '3']);
        $this->assertEquals($result, $number_work);
    }

}
