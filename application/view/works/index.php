
<div class="container">
    <h1>To Do List</h1>

    <!-- add work form -->
    <div class="box">
        <h3>Add a work</h3>
        <form class="form-inline" action="<?php echo URL; ?>works/addwork" method="POST">
            <div class="form-group mb-2">
                <label>Work Name : </label>
                <input type="text" class="form-control" name="work_name" value="" maxlength="255" required  />
            </div>
            <div class="form-group mb-2">
                <label>Start Date : </label>
                <input type="text" class="form-control" id="start_date" name="start_date" value="" required />
            </div>
            <div class="form-group mb-2">
                <label>End Date : </label>
                <input type="text" class="form-control" id="end_date" name="end_date" value="" required />
            </div>
            <div class="form-group mb-2">
                <label>Status : </label>
                <select class="form-control" id="status" name="status">
                    <?php
                    $status = Mini\Model\Work::getStatusArray();
                    foreach ($status as $statusCode => $statusText) {
                        echo "<option value=\"$statusCode\">$statusText</option>";
                    }
                    ?>
                </select>
            </div>
            <button type="submit" style="margin-right: 10px" class="btn btn-primary " name="submit_add_work" >Submit</button>

        </form>
    </div>
    <!-- main content output -->
    <div class="box">
        <h3>Amount of work to do : <?php echo $workNotComplete ?></h3>

        <div class="table-responsive">
            <table class="table thead-dark table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Work Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($works) > 0) {
                        foreach ($works as $work) { ?>
                            <tr>
                                <td><?php if (isset($work->id)) echo htmlspecialchars($work->id, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php if (isset($work->work_name)) echo htmlspecialchars($work->work_name, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php if (isset($work->start_date)) echo htmlspecialchars($work->start_date, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php if (isset($work->end_date)) echo htmlspecialchars($work->end_date, ENT_QUOTES, 'UTF-8'); ?></td>
                                <td><?php if (isset($work->status)) echo htmlspecialchars($status[$work->status], ENT_QUOTES, 'UTF-8'); ?></td>
                                <td>
                                    <a href="<?php echo URL . 'works/editwork/' . htmlspecialchars($work->id, ENT_QUOTES, 'UTF-8'); ?>">Edit</a> ||
                                    <a href="<?php echo URL . 'works/deletework/' . htmlspecialchars($work->id, ENT_QUOTES, 'UTF-8'); ?>" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                </td>
                            </tr>
                        <?php
                        }
                    } else {
                        echo "No Work to do . Create something";
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
