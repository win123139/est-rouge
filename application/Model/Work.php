<?php

/**
 * Class Work
 * This is a Work class.
 *
 */

namespace Mini\Model;

use Mini\Core\Model;
use DateTime;

class Work extends Model {

    /**
     * Constant use in model
     */
    const STATUS_PLANNING = 1;
    const STATUS_DOING = 2;
    const STATUS_COMPLETE = 3;
    const VALID = TRUE;
    const UNVALID = FALSE;

    /**
     * Validate work for create and update
     * @param string $work_name work name
     * @param string $start_date start date
     * @param string $end_date end date
     * @param int $status status
     */
    public function validate($work_name, $start_date, $end_date, $status) {
        $validate = self::VALID;
        // Validate work_name
        $work_name == "" ? $validate = self::UNVALID : '';
        // 255 max character work_name
        strlen($work_name) > 255 ? $validate = self::UNVALID : '';

        // Validate start_date
        $start_date == "" ? $validate = self::UNVALID : '';
        // Validate start_date format
        $date = DateTime::createFromFormat("Y-m-d H:i:s", $start_date);
        $date === false ? $validate = self::UNVALID : '';

        // Validate end_date
        $end_date == "" ? $validate = self::UNVALID : '';
        // Validate end_date format
        $date = DateTime::createFromFormat("Y-m-d H:i:s", $end_date);
        $date === false ? $validate = self::UNVALID : '';
        // Validate end_date greater than start_date
        strtotime($start_date) > strtotime($end_date) ? $validate = self::UNVALID : '';

        // Validate status
        !in_array($status, [Work::STATUS_PLANNING, Work::STATUS_DOING, Work::STATUS_COMPLETE]) ? $validate = self::UNVALID : '';

        return $validate;
    }

    /**
     * Get all tbl_works from database
     */
    public function getAllWorks() {
        $sql = "SELECT * FROM tbl_work";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Add a work to database
     * @param string $work_name work name
     * @param string $start_date start date
     * @param string $end_date end date
     * @param int $status status
     */
    public function addWork($work_name, $start_date, $end_date, $status) {
        $sql = "INSERT INTO tbl_work (work_name, start_date, end_date , status) VALUES (:work_name, :start_date, :end_date ,:status)";
        $query = $this->db->prepare($sql);
        $parameters = array(':work_name' => $work_name, ':start_date' => $start_date, ':end_date' => $end_date, ':status' => $status);

        $query->execute($parameters);
    }

    /**
     * Delete a tbl_work in the database
     * Please note: this is just an example! In a real application you would not simply let everybody
     * add/update/delete stuff!
     * @param int $tbl_work_id Id of tbl_work
     */
    public function deleteWork($work_id) {
        $sql = "DELETE FROM tbl_work WHERE id = :work_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':work_id' => $work_id);

        $query->execute($parameters);
    }

    /**
     * Get a tbl_work from database
     * @param integer $work_id
     */
    public function getWork($work_id) {
        $sql = "SELECT * FROM tbl_work WHERE id = :work_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':work_id' => $work_id);

        $query->execute($parameters);

        return ($query->rowcount() ? $query->fetch() : false);
    }

    /**
     * Update a tbl_work in database
     * @param string $work_id id
     * @param string $work_name work name
     * @param string $start_date start date
     * @param string $end_date end date
     * @param int $status status
     */
    public function updateWork($work_id, $work_name, $start_date, $end_date, $status) {
        $sql = "UPDATE tbl_work SET work_name = :work_name, start_date = :start_date, end_date = :end_date ,status = :status WHERE id = :work_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':work_name' => $work_name, ':start_date' => $start_date, ':end_date' => $end_date, ':status' => $status, ':work_id' => $work_id);

        $query->execute($parameters);
    }

    /**
     * Get number of work not complete
     * 
     */
    public function getAmountOfWorksNotComplete() {
        $sql = "SELECT COUNT(id) AS amount_of_tbl_works FROM tbl_work where status <> :status";
        $query = $this->db->prepare($sql);
        $parameters = array(':status' => self::STATUS_COMPLETE);
        $query->execute($parameters);

        // fetch() is the PDO method that get exactly one result
        return $query->fetch()->amount_of_tbl_works;
    }

    /**
     * Get array events for calendar
     * 
     */
    public function getWorksForCalendar() {
        $works = $this->getAllWorks();
        $result = [];
        if (count($works) > 0) {
            foreach ($works as $work) {
                $result[] = [
                    "id" => $work->id,
                    "title" => $work->work_name,
                    "start" => $work->start_date,
                    "end" => $work->end_date,
                    "url" => URL . 'works/editwork/' . $work->id
                ];
            }
        }
        
        return $result;
    }

    /**
     * Get list status
     * return array
     */
    public static function getStatusArray() {

        return [
            self::STATUS_PLANNING   => 'Planning',
            self::STATUS_DOING      => 'Doing',
            self::STATUS_COMPLETE   => 'Complete'
        ];
    }

}
