<?php

/**
 * Class WorksController
 *
 */

namespace Mini\Controller;

use Mini\Model\Work;

class WorksController
{
    /**
     * PAGE: index
     * 
     */
    public function index()
    {
        // Instance new Model (Work)
        $Work = new Work();
        // getting all works and amount of works
        $works = $Work->getAllWorks();
        $workNotComplete = $Work->getAmountOfWorksNotComplete();

       // load views. within the views we can echo out $works and $amount_of_works easily
        require APP . 'view/_templates/header.php';
        require APP . 'view/works/index.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * ACTION: addWork
     * This method handles what happens when you move to  /works/addwork
     */
    public function addWork()
    {
        // if we have POST data to create a new work entry
        if (isset($_POST["submit_add_work"])) {
            // Instance new Model (Work)
            $Work = new Work();
            // validate work
            if($Work->validate($_POST["work_name"], $_POST["start_date"],  $_POST["end_date"] , $_POST["status"]) == Work::VALID){
                // do addWork() in model/model.php
                $Work->addWork($_POST["work_name"], $_POST["start_date"],  $_POST["end_date"] , $_POST["status"]);
            }
        }

        // where to go after work has been added
        header('location: ' . URL . 'works/index');
    }

    /**
     * ACTION: deleteWork
     * This method handles what happens when you move to works/deletework
     * @param int $work_id Id of the to-delete work
     */
    public function deleteWork($work_id)
    {
       
        if (isset($work_id)) {
            // Instance new Model (Work)
            $Work = new Work();
            // do deleteWork() in model/Work.php
            $Work->deleteWork($work_id);
        }

        // where to go after work has been deleted
        header('location: ' . URL . 'works/index');
    }

     /**
     * ACTION: editWork
     * This method handles what happens when you move to /works/editwork
     * @param int $work_id Id of the to-edit work
     */
    public function editWork($work_id)
    {
        
        if (isset($work_id)) {
            // Instance new Model (Work)
            $Work = new Work();
            // do getWork() in model/Work.php
            $work = $Work->getWork($work_id);

            // If the work wasn't found, then it would have returned false, and we need to display the error page
            if ($work === false) {
                $page = new \Mini\Controller\ErrorController();
                $page->index();
            } else {
                // load views. within the views we can echo out $work easily
                require APP . 'view/_templates/header.php';
                require APP . 'view/works/edit.php';
                require APP . 'view/_templates/footer.php';
            }
        } else {
            // redirect user to works index page (as we don't have a work_id)
            header('location: ' . URL . 'works/index');
        }
    }

    /**
     * ACTION: updateWork
     * This method handles what happens when you move to /works/updatework
     */
    public function updateWork()
    {
        // if we have POST data to create a new work entry
        if (isset($_POST["submit_update_work"])) {
            // Instance new Model (Work)
            $Work = new Work();
            // validate work
            if($Work->validate($_POST["work_name"], $_POST["start_date"],  $_POST["end_date"] , $_POST["status"]) == Work::VALID){
            // do updateWork() from model/Work.php
            $Work->updateWork($_POST["work_id"], $_POST["work_name"],  $_POST["start_date"], $_POST['end_date'] ,$_POST['status']);
            }
        }

        // where to go after work has been added
        header('location: ' . URL . 'works/index');
    }



}
