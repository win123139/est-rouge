<?php

/**
 * Class HomeController
 *
 * 
 */

namespace Mini\Controller;
use Mini\Model\Work;
class HomeController
{
    /**
     * PAGE: index
     * 
     */
    public function index()
    {
        // Instance new Model (Work)
        $Work = new Work();
        // getting all works 
        $works = $Work->getWorksForCalendar();
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/footer.php';
    }

   
}
